import request from '../utils/request';

const authService = {
    login: (username, password) => {
        return request.post('/auth/login', { username: username, password: password })
            .then((response) => {
                const token = response.data.token;
                localStorage.setItem('token', token);
                return true;
            })
            .catch(err => {
                if (err.response.status === 401) {
                    return false;
                }
            });
    },
    logout: () => {
        localStorage.removeItem('token');
    },
    isAuthenticated: () => {
        const token = localStorage.getItem('token');        
        return token ? true : false;
    },
    getUserRoles: () => {
        return ['ADMIN'];
    }
} 

export default authService;