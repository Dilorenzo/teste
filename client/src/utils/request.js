import axios from "axios";
import { HTTP_STATUS_CODE, SERVER_VALIDATION_CODE } from '../utils/constants';
import router from "../router";

const request = axios.create({
    baseURL: '/api',
});

request.interceptors.request.use(config => {
    return { ...config, headers: { ...config.headers, Authorization: localStorage.getItem("token") } }
});

request.interceptors.response.use((response) => {
    return response
  }, (error) => {    
    if (error.response.status === HTTP_STATUS_CODE.UNAUTHORIZED) {
      if (SERVER_VALIDATION_CODE.INVALID_CREDENTIALS === error.response.data.code) {
        throw error;
      } else {        
        localStorage.removeItem("token")
        window.location = ""
      }
    } else if (error.response.status == HTTP_STATUS_CODE.INTERNAL_SERVER_ERROR) {
        router.push('/error')
    } else {
      throw error
    }
  })
  

export default request;