import Login from './Login.vue';
import { shallowMount, mount } from '@vue/test-utils';

describe('Login page', () => {
    it('should show mandatory fields message if not filled', async () => {
        const wrapper = shallowMount(Login)

        const loginInput = wrapper.get('[data-test="input-username"]');
        expect(loginInput.exists()).toBe(true);
        const loginButton = wrapper.get('[data-test="button-login"]')
        expect(loginButton.exists()).toBe(true);
        await loginButton.trigger('click');
        const validationErrors = wrapper.findAll('[data-test="small-validation-error"');
        expect(validationErrors.length).toBe(2);
    });

    it('should show mandatory field message if not filled', async () => {
        const wrapper = mount(Login)
        const loginInput = wrapper.get('[data-test="input-username"]');
        expect(loginInput.exists()).toBe(true);        
        loginInput.element.value = 'testing';
        await loginInput.trigger('input');
        const loginButton = wrapper.get('[data-test="button-login"]');
        expect(loginButton.exists()).toBe(true);
        await loginButton.trigger('click');
        expect(wrapper.vm.$v.email.$invalid).toBe(true) 
        const validationErrors = wrapper.findAll('[data-test="small-validation-error"');
        expect(validationErrors.length).toBe(1);
    });
});