export const ROUTE_NAMES = {
    INDEX: 'login',
}

export const  ROUTES_DEFINITION = {
    path: '/login',
    name: 'login',
    component: () => import('./Login.vue')
};