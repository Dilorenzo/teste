from model.user import User

def verify_credentials(username, password):
    user = User.query.filter_by(username = username).first()
    return user and user.password == password
        
