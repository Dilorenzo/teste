from model.user import User, users_schema, user_schema
from model import db


def find_users_paginated(page=1, per_page=10) -> dict:
    data = User.query.paginate(page=page, per_page=per_page)
    return dict(items=users_schema.dump(data.items), total=data.total, current_page=data.page, per_page=data.per_page)

def create_user(data) -> dict:
    #Verify if username already exists.
    user = user_schema.load(data)
    db.session.add(user)
    db.session.commit()
    return user_schema.dump(user)