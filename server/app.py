from util import create_app

app = create_app('app.cfg')

if __name__ == '__main__':
    app.run()