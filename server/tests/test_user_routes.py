import json

def test_get_users(client, request_jwt_header):    
    response = client.get('/users/', headers = request_jwt_header)
    assert response.status_code == 200
    res = json.loads(response.data)
    assert res.get('current_page') == 1
    