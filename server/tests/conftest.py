import pytest
from util import create_app
import logging
from flask_jwt_extended import create_access_token

logging.basicConfig(level=logging.DEBUG)
mylogger = logging.getLogger(__name__)

@pytest.fixture
def app():
    app = create_app('app.cfg')

    yield app

@pytest.fixture()
def client(app):
    return app.test_client()

@pytest.fixture()
def runner(app):
    return app.test_cli_runner()

@pytest.fixture()
def request_jwt_header():
    access_token = create_access_token('testuser')
    header = {
        'Authorization': 'Bearer {}'.format(access_token)
    }
    return header