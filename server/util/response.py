from flask import jsonify, make_response

CODE_UNAUTHORIZED = 401

class ApiResponse:    

    def __init__(self, data, code):
        self._data = data
        self._code = code

    def build_reponse(self):
        return jsonify(self._data), self._code

    @classmethod
    def OK(cls, data: dict):
        return jsonify(data)

    @classmethod
    def UNAUTHORIZED(cls, message: str, error_code: str) -> tuple:
        return jsonify(dict(msg=message, code=error_code)), CODE_UNAUTHORIZED