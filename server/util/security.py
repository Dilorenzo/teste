from flask_jwt_extended import verify_jwt_in_request, get_jwt
from functools import wraps
from flask import jsonify

def has_roles(role_list):
    def decorator(fn):
        @wraps(fn)
        def wrapper(*args, **kwargs):
            verify_jwt_in_request()
            claims = get_jwt()
            role = 'role' in claims and claims['role']
            if role in role_list:
                return fn(*args, **kwargs)
            else:
                return jsonify(msg = 'Access denied'), 403
        return wrapper
    return decorator