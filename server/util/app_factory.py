from flask import Flask
from flask_jwt_extended import JWTManager


def create_app(config_filename=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_pyfile(config_filename)    
    jwt = JWTManager()
    from model import db, ma, migrate
    jwt.init_app(app)
    db.init_app(app)
    migrate.init_app(app, db)
    ma.init_app(app)

    from routes import user_bp, auth_bp
    app.register_blueprint(user_bp)
    app.register_blueprint(auth_bp)
    #from handlers import *
    return app