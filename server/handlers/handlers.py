from app import app
from jsonschema import ValidationError
from flask import jsonify
from util.response import ApiResponse

@app.errorhandler(400)
def bad_request(error):
    if isinstance(error.description, ValidationError):
        original_error = error.description        
        return jsonify({'error': original_error.message}), 400
    return error


@app.errorhandler(401)
def unauthorized(error):
    if isinstance(error.description, ValidationError):
        original_error = error.description
        print(original_error.path)
        return ApiResponse.UNAUTHORIZED('Invalid credentials', 'TOKEN_EXPIRED')
    return error