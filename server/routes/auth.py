from flask import Blueprint, request, jsonify
from flask_jwt_extended import create_access_token
from flask_expects_json import expects_json
from services.auth import verify_credentials
from util.response import ApiResponse

schema = {
  "type": "object",
  "properties": {
    "username": { "type": "string" },
    "password": { "type": "string" }
  },
  "required": ["username", "password"]
}

auth_bp = Blueprint('auth', __name__, url_prefix='/api/auth')

@auth_bp.route('/login', methods=['POST'])
@expects_json(schema)
def login():
    username = request.json.get('username', None)
    password = request.json.get('password', None)    
    if not verify_credentials(username, password):
        return ApiResponse.UNAUTHORIZED("Bad username or password", "INVALID_CREDENTIALS")
    access_token = create_access_token(identity=username, additional_claims={'role': 'admin'})
    return ApiResponse.OK(dict(token=access_token))