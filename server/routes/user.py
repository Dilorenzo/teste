from flask import Blueprint, request
from flask_jwt_extended import jwt_required
from flask_expects_json import expects_json
from services.user import find_users_paginated, create_user
from util import has_roles

schema = {
  "type": "object",
  "properties": {
    "username": { "type": "string" },
    "password": { "type": "string" }
  },
  "required": ["username", "password"]
}

user_bp = Blueprint('users', __name__, url_prefix='/users')

@user_bp.route('/', methods=['GET'])
@jwt_required()
@has_roles(['admin'])
def get_users():
    page = request.args.get('page', 1, type=int)
    per_page = request.args.get('per_page', 1, type=int)    
    return find_users_paginated(page, per_page)

@user_bp.route('/', methods=['POST'])
@jwt_required()
@has_roles(['admin'])
@expects_json(schema)
def post_user():    
    return create_user(request.json)